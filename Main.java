import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> array1 = new ArrayList<>();
        array1.add(-2);
        array1.add(-3);
        array1.add(6);
        array1.add(3);

        List<Integer> array2 = new ArrayList<>();
        array2.add(-2);
        array2.add(4);
        array2.add(5);
        array2.add(-3);

        List<Integer> array3 = new ArrayList<>();

        array3.addAll(array1);
        array3.addAll(array2);

        System.out.println("Array1 : "+array1);
        System.out.println("Array2 : "+array2);
        System.out.println("Array3 is Array1 & Array2 Merged : "+array3);


        int mnValue=array3.get(0);
        for(int i=0; i<= array3.size()-1; i++){
           if( array3.get(i)<mnValue);{
                mnValue= array3.get(i);
            }
        }
            System.out.println("Minimum value in Array3 is :" + mnValue);

        // Also found this other way of finding min
        int minValue=array3.stream().min(Integer::compare).get();
        System.out.println("Minimum value in Array3 is" +minValue);

    }
}